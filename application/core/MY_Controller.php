<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Controller extends MX_Controller {

    public $data;

    function __construct() {
        parent:: __construct();
        date_default_timezone_set("Asia/Kolkata");
        
        $this->data['admin_assets'] = base_url('/assets-admin/');
        
           
    }//constructor
}