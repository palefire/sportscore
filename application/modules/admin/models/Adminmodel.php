<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Adminmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }


    /**
     * Checklogin FUNCTION
     */
    public function checklogin( $username, $password){
        $q = $this->db
                    ->where( array('username'=>$username) )
                    ->get('admin_auth');
        $result = $q->result_array();
        if( $q->num_rows() > 0 ){
            $password_db = $result[0]['password'];
            if(password_verify( $password, $password_db)) {
                return $result[0];
            }else{
                return FALSE;
            }

        }else{
            return FALSE; 
        }
    }//fn

     /**
     * Check Remember Me FUNCTION
     */
    public function check_remember_me( $token){
        if( $token != '' ){
            $q = $this->db
                    ->where( array('remember_me_token'=>$token) )
                    ->get('admin_auth');
            $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }//fn

     /**
     * Create Slug FUNCTION
     */
    public function create_slug( $name ){
        $slug = strtolower($name); 
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        return $slug;
    }//fn

    public function get_admin_by_id( $admin_id ){
        $q = $this->db
                    ->where( array('admin_id'=>$admin_id) )
                    ->get('admin_auth');
        $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Category function
    |--------------------------------------------------------------------------
    */

    //check if cat slug exists?
    public function check_if_cat_slug_exists( $cat_slug ){
        $q = $this->db
                    ->like('cat_slug', $cat_slug)
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            $exist_number = $q->num_rows();
            $cat_slug = $cat_slug.'-'.$exist_number;
            return $cat_slug;
        }else{
            return $cat_slug;
        }
    }//fn

    public function get_cat_list(){
        $q = $this->db
                    ->select('wst_category.cat_id,wst_category.cat_slug,wst_category.cat_name,wst_category.cat_count')
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Website function
    |--------------------------------------------------------------------------
    */
    
    public function get_user_list(){
        $q = $this->db
                    ->select('*')
                    ->get('website_auth');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }
    }//fn

    public function get_epl_table($season){
        $table_name = 'spsc_epl'.$season.'_table';
        $q = $this->db
                    ->select('*')
                    ->get(strval($table_name));
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }
    }//fn
    public function get_ipl_table($season){
        $table_name = 'spsc_ipl'.$season.'_table';
        $q = $this->db
                    ->select('*')
                    ->get(strval($table_name));
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }
    }//fn

    public function check_user_access( $email, $secret_key ){
        $q = $this->db
                    ->select('website_auth.website_id,website_auth.expiration_date')
                    ->where(array('email' => $email, 'secret_key' => $secret_key))
                    ->get('website_auth');
        if( $q->num_rows() > 0 ){
            return $q->result_array()[0]['expiration_date'];  
        }else{
            return false;
        }
    }//fn

    public function create_connection_key( $email ){
        $connection_key = password_hash($email, CRYPT_BLOWFISH);
        $update = $this->db->update('website_auth', array('connection_key'=> $connection_key,'verified' =>  '1'), array('email' => $email));
        if($update){
            return $connection_key;
        }else{
            return false;
        }
    }//fn
    public function check_api_expiration($email){

    }
}//class
