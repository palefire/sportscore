<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        if( !isset($this->session->userdata['spsc23xyzsdfretw89lk_admin_id']) ){
			return redirect('admin/login');
			
		}
        
         $this->load->model('adminmodel');
         $admin_details = $this->adminmodel->get_admin_by_id( $this->session->userdata['spsc23xyzsdfretw89lk_admin_id'] );
         $this->data['admin_details'] = $admin_details;
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('dashboard');
		$this->load->view('footer');
		
	}//fn

	/*
	|--------------------------------------------------------------------------
	| EPL function
	|--------------------------------------------------------------------------
	*/
	public function epl($season){

		$league_table = $this->adminmodel->get_epl_table($season);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('epl/epl', array(
			'league_table'	=> $league_table,
			'season'		=> $season
		));
		$this->load->view('footer');
		
	}//fn

	public function update_epl_table_2020(){
		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => "https://api-football-beta.p.rapidapi.com/standings?season=2020&league=39",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"x-rapidapi-host: api-football-beta.p.rapidapi.com",
				"x-rapidapi-key: 4957d03d4cmshe0a007f9417d1a4p19ea0fjsn85c6464000df"
			],
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			// echo $response;
			$datas = json_decode($response);
			$table_datas = $datas->response[0]->league->standings[0];
			
			$truncate = $this->db->query( "TRUNCATE TABLE `spsc_epl2020_table`");
			// print_r($datas->response[0]->league->standings);
			if($truncate){
				// print_r($table_datas);
				// $counter = 0;
				foreach($table_datas as $data){
					$data = array(
	                                'team_id' => $data->team->id,
	                                'name' => $data->team->name,
	                                'logo_url' => $data->team->logo,
	                                'position' => $data->rank,
	                                'played' => $data->all->played,
	                                'won' => $data->all->win,
	                                'drawn' => $data->all->draw,
	                                'lost' => $data->all->lose,
	                                'points' => $data->points,
	                                'goal_difference' => $data->goalsDiff,
	                                'goal_for' => $data->all->goals->for,
	                                'goal_against' => $data->all->goals->against,
	                                );
					$this->db->insert('spsc_epl2020_table', $data);
					// $counter++;
				}//foreach
				
				echo 1;
				// echo json_encode($dele);
				die();
			}else{
				echo 0;
				die();
			}
		}
	}//fn
	public function update_epl_table_2019(){
		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => "https://api-football-beta.p.rapidapi.com/standings?season=2019&league=39",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"x-rapidapi-host: api-football-beta.p.rapidapi.com",
				"x-rapidapi-key: 4957d03d4cmshe0a007f9417d1a4p19ea0fjsn85c6464000df"
			],
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			// echo $response;
			$datas = json_decode($response);
			$table_datas = $datas->response[0]->league->standings[0];
			
			$truncate = $this->db->query( "TRUNCATE TABLE `spsc_epl2019_table`");
			// print_r($datas->response[0]->league->standings);
			if($truncate){
				// print_r($table_datas);
				// $counter = 0;
				foreach($table_datas as $data){
					$data = array(
	                                'team_id' => $data->team->id,
	                                'name' => $data->team->name,
	                                'logo_url' => $data->team->logo,
	                                'position' => $data->rank,
	                                'played' => $data->all->played,
	                                'won' => $data->all->win,
	                                'drawn' => $data->all->draw,
	                                'lost' => $data->all->lose,
	                                'points' => $data->points,
	                                'goal_difference' => $data->goalsDiff,
	                                'goal_for' => $data->all->goals->for,
	                                'goal_against' => $data->all->goals->against,
	                                );
					$this->db->insert('spsc_epl2019_table', $data);
					// $counter++;
				}//foreach
				
				echo 1;
				// echo json_encode($dele);
				die();
			}else{
				echo 0;
				die();
			}
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| IPL function
	|--------------------------------------------------------------------------
	*/	
	public function ipl($season){

		$league_table = $this->adminmodel->get_ipl_table($season);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('ipl/ipl', array(
			'league_table'	=> $league_table,
			'season'		=> $season
		));
		$this->load->view('footer');
		
	}//fn

	public function update_ipl_table_2020(){
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => "https://dev132-cricket-live-scores-v1.p.rapidapi.com/seriesstandings.php?seriesid=2780",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"x-rapidapi-host: dev132-cricket-live-scores-v1.p.rapidapi.com",
				"x-rapidapi-key: 4957d03d4cmshe0a007f9417d1a4p19ea0fjsn85c6464000df"
			],
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			// echo $response;
			$datas = json_decode($response);
			$table_datas = $datas->teams;
			$truncate = $this->db->query( "TRUNCATE TABLE `spsc_ipl2020_table`");
			// print_r($datas->response[0]->league->standings);
			if($truncate){
				foreach($table_datas as $data){
					$data = array(
	                                'team_id' => $data->id,
	                                'name' => $data->name,
	                                'short_name' => $data->shortName,
	                                'logo_url' => $data->logoUrl,
	                                'position' => $data->position,
	                                'played' => $data->played,
	                                'won' => $data->won,
	                                'drawn' => $data->drawn,
	                                'lost' => $data->lost,
	                                'points' => $data->points,
	                                'netRunRate' => $data->netRunRate,
	                                );
					$this->db->insert('spsc_ipl2020_table', $data);
					// $counter++;
				}//foreach
				
				echo 1;
				// echo json_encode($dele);
				die();
			}else{
				echo 0;
				die();
			}
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Category function
	|--------------------------------------------------------------------------
	*/
	public function all_cat(){
		$all_cat = $this->adminmodel->get_cat_list();
		// print_r($all_cat);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('category/all_cat', array(
			'cat_list' => $all_cat,
		));
		$this->load->view('footer');
	}//fn

	public function add_cat_to_db(){
		$cat_name = $this->input->post('cat_name');
		// echo $cat_name;
		$cat_slug_raw = $this->adminmodel->create_slug($cat_name);
		$cat_slug = $this->adminmodel->check_if_cat_slug_exists($cat_slug_raw);
		// echo $cat_slug;	
		$data = array(
			'cat_slug' 			=> $cat_slug,
			'cat_name'			=> $cat_name,
			'cat_count'			=> 0,
			'cat_created_by'	=> $this->session->userdata['spsc23xyzsdfretw89lk_admin_id']
		);
		$insert = $this->db->insert('spsc_category', $data);
		if( $insert ){
			$this->session->set_flashdata( 'cat_addition_success' , 'Category added succesfully' );
			return redirect('admin/all_cat');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
			return redirect('admin/all_cat');
		}
	}//fn

	public function edit_cat_in_db(){
		$cat_name = $this->input->post('cat_name');
		$cat_id = $this->input->post('cat_id');
		$data = array(
			'cat_name'			=> $cat_name,
		);
		$update = $this->db->update('spsc_category',$data, array('cat_id'=> $cat_id));
		if( $update ){
			$mod_data = array(
				'cat_id'		=> $cat_id,
				'modified_by'	=> $this->session->userdata['spsc23xyzsdfretw89lk_admin_id'],
			);
			$insert = $this->db->insert('spsc_category_modification', $mod_data);
			$this->session->set_flashdata( 'cat_addition_success' , 'Category updated succesfully' );
			return redirect('admin/all_cat');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot edit !' );
			return redirect('admin/all_cat');
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Users function
	|--------------------------------------------------------------------------
	*/
	public function all_users(){
		$user_list = $this->adminmodel->get_user_list();
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('users/all_users', array(
			'user_list'	=> $user_list
		));
		$this->load->view('footer');
		
	}//fn

	public function single_user( $username ){
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('users/single_user');
		$this->load->view('footer');
	}//fn

	public function pdf_test(){
		$data_user = array();
		$data_user['users'] = $this->adminmodel->get_user_list();
		$this->load->view('invoice_pdf',$data_user);
		$html = $this->output->get_output();
		$this->load->library('pdf');
		$this->dompdf->loadHtml($html);
		$this->dompdf->setPaper('A4', 'landscape');
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
	}

	

}//class