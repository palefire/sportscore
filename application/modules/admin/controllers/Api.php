<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        $this->apiuser = 'sarasij94';
        $this->apipass = '123';
        $this->response_text = array(0 => 'Fail', 1 => 'Success', 9 => 'Authentication Error', 404 => 'Page Not Found!');

        $this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
      
	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Connection Api
	|--------------------------------------------------------------------------
	*/
	public function connect(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$email 					= $all_data->email;
			$secret_key 			= $all_data->secret_key;
			
			$check_user_access = $this->adminmodel->check_user_access( $email, $secret_key );
			if($check_user_access){
				$create_connection_key 	= $this->adminmodel->create_connection_key( $email );
				if( $create_connection_key ){
					if( time() < strtotime($check_user_access) ){
						$array = array(
							"code" 			=> 1,
							"message" 		=> 'Success',
							'data'			=> $create_connection_key,
							'expiration'	=> $check_user_access
						);
						echo json_encode($array);
					}else{
						$array = array(
							"code" => 7,
							"message" => 'Subscription expired'
						);
						echo json_encode($array);
					}	
				}else{
					$array = array(
						"code" => 0,
						"message" => 'Error while creating connection key.'
					);
					echo json_encode($array);
				}
			}else{
				$array = array(
					"code" => 5,
					"message" => 'Wrong access code'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| IPL Api
	|--------------------------------------------------------------------------
	*/
	
	public function get_ipl_table(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$email 			= $all_data->email;
			$secret_key 	= $all_data->secret_key;
			$season 	= $all_data->season;
			$expiry_date = $this->adminmodel->check_user_access( $email, $secret_key );
			if($expiry_date){
				if( time() < strtotime($expiry_date) ){
					$league_table = $this->adminmodel->get_ipl_table($season);
					$array = array(
						"code" => 1,
						"message" => 'success',
						'data'		=> $league_table
					);
					echo json_encode($array);
				}else{
					$array = array(
						"code" => 7,
						"message" => 'Subscription expired'
					);
					echo json_encode($array);
					}	
			}else{
				$array = array(
					"code" => 5,
					"message" => 'Wrong access code'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Sample Api
	|--------------------------------------------------------------------------
	*/
	public function sample(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Image Sample Api
	|--------------------------------------------------------------------------
	*/
	public function image_sample(){
		// $all_data_json = file_get_contents('php://input');
		$all_data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		if (!isset($all_data['apiuser']) || !isset($all_data['apipass'])) {
            $return_res = array('code' => 9, 'message' => $this->response_text[9]);
            echo json_encode($return_res);
            exit();
        }elseif( $all_data['apiuser'] != $this->apiuser OR $all_data['apipass'] != $this->apipass ){
        	$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
        }else{
        	$user_id 	= $all_data['apius'];
			
        }

	}//fn
	/*
	|--------------------------------------------------------------------------
	| Card Api
	|--------------------------------------------------------------------------
	*/
	public function cron(){
		$otp = rand(1000,9999);
        $otp_data = array(
            'otp_user_id'   => '123456',
            'otp_phone'     => '1234567890',
            'otp_value'     => $otp,
        );
        $this->db->insert('wokk_otp', $otp_data);

	}//



}//class