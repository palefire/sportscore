<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">IPL <?php echo $season ?></h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/')  ?>" class="breadcrumb-link">Dashboard</a></li>
                                
                                <li class="breadcrumb-item active" aria-current="page">IPL <?php echo $season ?></li>
                            </ol>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Table</h5>
                        <p>League Table IPL <?php echo $season ?>. <button class="btn btn-primary" id="update-ipl<?php echo $season ?>-table" style="float: right">Update</button></p>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered " style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Position</th>
                                        <th>Logo</th>
                                        <th>Name</th>
                                        <th>Points</th>
                                        <th>Played</th>
                                        <th>Won</th>
                                        <th>Drawn</th>
                                        <th>Lost</th>
                                        <th>Net Runrate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if($league_table){
                                        foreach( $league_table as $row ){
                                    ?>
                                            <tr>
                                                <td><?php echo $row['position'] ?></td>
                                                <td><img src="<?php echo $row['logo_url'] ?>" style="height: 40px"></td>
                                                <td><?php echo $row['name'] ?></td>
                                                <td><span class="badge badge-success"><?php echo $row['points'] ?></span></td>
                                                <td><?php echo $row['played'] ?></td>
                                                <td><?php echo $row['won'] ?></td>
                                                <td><?php echo $row['drawn'] ?></td>
                                                <td><?php echo $row['lost'] ?></td>
                                                <td><?php echo $row['netRunRate'] ?></td>
                                            </tr>
                                    <?php
                                                }
                                        }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Position</th>
                                        <th>Logo</th>
                                        <th>Name</th>
                                        <th>Points</th>
                                        <th>Played</th>
                                        <th>Won</th>
                                        <th>Drawn</th>
                                        <th>Lost</th>
                                        <th>Net Runrate</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>     
    </div> 
    <script type="text/javascript">
        $(document).ready(function(){
            $("#update-ipl2020-table").on('click', function(e){
                e.preventDefault();
                var that = $(this);
                $.ajax({
                    beforeSend : function(xhr){
                        
                    },
                    url : "<?php echo site_url('admin/update_ipl_table_2020') ?>",
                    type : 'POST',
                    data : {
                        
                    },
                    success: function( data ){
                        console.log(data);
                        if(data == 1){
                            location.reload();
                        }
                    },
                    error: function(response){
                        console.log(response);
                    }
                  });
            });
            $("#update-ipl2019-table").on('click', function(e){
                e.preventDefault();
                var that = $(this);
                $.ajax({
                    beforeSend : function(xhr){
                        
                    },
                    url : "<?php echo site_url('admin/update_ipl_table_2019') ?>",
                    type : 'POST',
                    data : {
                        
                    },
                    success: function( data ){
                        console.log(data);
                        if(data == 1){
                            location.reload();
                        }
                    },
                    error: function(response){
                        console.log(response);
                    }
                  });
            });
        });
    </script> 
