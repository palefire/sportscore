<?php
    $status = array(
        '0' =>'<span class="badge badge-warning">New Unverified</span>',
        '1' =>'<span class="badge badge-success">Verified</span>',
        '2' =>'<span class="badge badge-success">Expired</span>',
    );
    $package = array(
        '0' => 'Free Trial',
        '1' => 'Indian Package',
        '2' => 'World Package',
    );
?>
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">All Users</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/')  ?>" class="breadcrumb-link">Dashboard</a></li>
                                
                                <li class="breadcrumb-item active" aria-current="page">All Users</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Data Tables - Print, Excel, CSV, PDF Buttons</h5>
                        <p>This is the list of all websites and their repective emails. </p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Website</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Package</th>
                                        <th>Status</th>
                                        <th>Expiration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($user_list as $user){
                                    ?>
                                            <tr>
                                                <td><?php echo $user['website_id'] ?></td>
                                                <td><a href="<?php echo site_url('/admin/single_user/'). $user['website_id'] ?>"><?php echo $user['website_url'] ?></a></td>
                                                <td><?php echo $user['first_name'].' '.$user['last_name'] ?></td>
                                                <td><?php echo $user['email'] ?></td>
                                                <td><?php echo $package[$user['package']] ?></td>
                                                <td><?php echo $status[$user['verified']] ?></td>
                                                <td><?php echo $user['email'] ?></td>
                                            </tr>
                                    <?php
                                        }
                                    ?>
                                   

                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Website</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Package</th>
                                        <th>Status</th>
                                        <th>Expiration</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>     
    </div>  
