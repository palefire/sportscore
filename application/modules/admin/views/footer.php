 <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             Copyright © 2020 Walledstory. All rights reserved. Walledstory <a href="https://thepalefire.com/">Palefire</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    
    <!-- bootstap bundle js -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <script src="<?php echo site_url('assets-admin/vendor/bootstrap/js/bootstrap.bundle.js') ?>"></script>
    <!-- slimscroll js -->
    <script src="<?php echo site_url('assets-admin/vendor/slimscroll/jquery.slimscroll.js') ?>"></script>
    <!-- main js -->
    <script src="<?php echo site_url('assets-admin/libs/js/main-js.js') ?>"></script>
    <!-- chart chartist js -->
    <script src="<?php echo site_url('assets-admin/vendor/charts/chartist-bundle/chartist.min.js') ?>"></script>
    <!-- sparkline js -->
    <script src="<?php echo site_url('assets-admin/vendor/charts/sparkline/jquery.sparkline.js') ?>"></script>
    <!-- morris js -->
    <script src="<?php echo site_url('assets-admin/vendor/charts/morris-bundle/raphael.min.js') ?>"></script>
    <script src="<?php echo site_url('assets-admin/vendor/charts/morris-bundle/morris.js') ?>"></script>
    <!-- chart c3 js -->
    <script src="<?php echo site_url('assets-admin/vendor/charts/c3charts/c3.min.js') ?>"></script>
    <script src="<?php echo site_url('assets-admin/vendor/charts/c3charts/d3-5.4.0.min.js') ?>"></script>
    <script src="<?php echo site_url('assets-admin/vendor/charts/c3charts/C3chartjs.js') ?>"></script>
    <script src="<?php echo site_url('assets-admin/libs/js/dashboard-ecommerce.js') ?>"></script>


    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url('/assets-admin/vendor/datatables/js/buttons.bootstrap4.min.js') ?>"></script>
    <script src="<?php echo site_url('/assets-admin/vendor/datatables/js/data-table.js') ?>"></script>
    <script src="<?php echo site_url('/assets-admin/vendor/datatables/js/dataTables.bootstrap4.min.js') ?>"></script>
    <script src="<?php echo site_url('/assets-admin/vendor/multi-select/js/jquery.multi-select.js') ?>"></script>


    
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>



</body>
 
</html>


