<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Frontmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /*
    |--------------------------------------------------------------------------
    | Login function
    |--------------------------------------------------------------------------
    */
    /**
     * Checklogin FUNCTION
     */
    public function checklogin( $username, $password){
        $q = $this->db
                    ->where( array('email'=>$username) )
                    ->get('website_auth');
        $result = $q->result_array();
        if( $q->num_rows() > 0 ){
            $password_db = $result[0]['password'];
            if(password_verify( $password, $password_db)) {
                return $result[0];
            }else{
                return FALSE;
            }

        }else{
            return FALSE; 
        }
    }//fn

     /**
     * Check Remember Me FUNCTION
     */
    public function check_remember_me( $token){
        if( $token != '' ){
            $q = $this->db
                    ->where( array('remember_me_token'=>$token) )
                    ->get('wst_admin');
            $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }//fn

     /**
     * Create Slug FUNCTION
     */
    public function create_slug( $name ){
        $slug = strtolower($name); 
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        return $slug;
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Register function
    |--------------------------------------------------------------------------
    */

    public function generate_secret_key(  ){
        $secret_key = substr(str_shuffle(md5(time())), 0, 32);;
        $q = $this->db
                    ->where( array('secret_key'=>$secret_key) )
                    ->get('website_auth');
        if( $q->num_rows() > 0 ){
            $this->generate_secret_key();
        }else{
            return $secret_key;
        }
    }//fn

    public function register_website_to_db( $email, $password, $url, $fname, $lname){
        $secret_key = $this->generate_secret_key();
        $data1 = array(
            'email'             => $email,
            'website_url'       => $url,
            'verified'          => '1',
            'secret_key'        => $secret_key,
            'password'          => password_hash($password, CRYPT_BLOWFISH),
            'first_name'        => $fname,
            'last_name'         => $lname,
            'package'           => '0',
            'is_live'           => '1',
            'expiration_date'   => date("Y/m/d H:i:s", strtotime("+ 7 days")),
        );
        $insert1 = $this->db->insert('website_auth', $data1);
        $website_id = $this->db->insert_id();
        if( $insert1 ){
            return $website_id;
        }else{
            return false;
        }
        
    }//fn
    
    public function check_email_exist( $email ){
        $q = $this->db
                    ->where( array('email'=>$email) )
                    ->get('website_auth');
        if( $q->num_rows() > 0 ){
            return TRUE;
        }else{
            return FALSE;
        }
    }//fn
    public function check_url_exist( $url ){
        $q = $this->db
                    ->where( array('website_url'=>$url) )
                    ->get('website_auth');
        if( $q->num_rows() > 0 ){
            return TRUE;
        }else{
            return FALSE;
        }
    }//fn

    public function verify_email( $user_token, $otp ){
        $q1 = $this->db
                    ->where(array('user_token' => $user_token, 'otp'=> $otp))
                    ->get('user_registration_otp');
        if( $q1->num_rows()>0 ){
            $update_user_auth_table = $this->db->update('user_authentication', array('user_status' => '1'), array('user_token' => $user_token));
            if( $update_user_auth_table ){
                $delete_otp = $this->db->delete('user_registration_otp', array('user_token'=> $user_token));
                if( $delete_otp ){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{  
            return false;
        }
    }//fn

    public function get_user_info( $website_id ){
        $q = $this->db
                    ->select('*')
                    ->where( array('website_id'=> $website_id) )
                    ->get('website_auth');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return false;
        }
    }//fn
  
}//class
