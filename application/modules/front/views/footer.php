

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>Walledsports</strong>. All Rights Reserved
          </div>
          <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Home</a>
            <a href="#about" class="scrollto">About</a>
            <a href="#">Privacy Policy</a>
            <a href="#">Terms of Use</a>
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo site_url('/') ?>assets/vendor/aos/aos.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/purecounter/purecounter.js"></script>
  <script src="<?php echo site_url('/') ?>assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo site_url('/') ?>assets/js/main.js"></script>
  <script src="<?php echo site_url('/') ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript">
  $(document).ready( function(){
    $('#login-btn').on('click', function(e){
      e.preventDefault();
      var that = $(this);
      that.parent('.text-center').siblings('.my-3').children('.error-message').hide();
      that.parent('.text-center').siblings('.my-3').children('.loading').hide();
      that.parent('.text-center').siblings('.my-3').children('.sent-message').hide();
      var username = that.parent('.text-center').siblings('.form-group').children('#username').val();
      var password = that.parent('.text-center').siblings('.form-group').children('#login-password').val();
      if( username.length < 1 ){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Please Provide the username or the email!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        return false;
      }

      if( password.length < 1 ){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Password Cannot be empty!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        return false;
      }
      $.ajax({
        beforeSend : function(xhr){
            that.parent('.text-center').siblings('.my-3').children('.loading').show();
        },
        url : "<?php echo site_url('front/login/verify_login') ?>",
        type : 'POST',
        data : {
            'username' : username,
            'password' : password
        },
        success: function( data ){
            console.log(data);
            var response = jQuery.parseJSON(data);
            if(response.code == 1){
              // that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.sent-message').show();
              setTimeout(
                function() 
                {
                  location.reload();
                }, 1000);
            }
            if(response.code == 0){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.error-message').html('Wrong email or password!');
              that.parent('.text-center').siblings('.my-3').children('.error-message').show();
            }
            if(response.code == 5){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.error-message').html('Already Logged in!');
              that.parent('.text-center').siblings('.my-3').children('.error-message').show();
            }
        },
        error: function(response){
            console.log(response);
        }
      });//ajax
    });

    $('#logout-btn').on('click', function(e){
      e.preventDefault();
      var that = $(this);
      $.ajax({
        beforeSend : function(xhr){
            that.parent('.text-center').siblings('.my-3').children('.loading').show();
        },
        url : "<?php echo site_url('front/login/ajax_logout') ?>",
        type : 'POST',
        data : {
            
        },
        success: function( data ){
            console.log(data);
            var response = jQuery.parseJSON(data);
            if(response.code == 1){
              location.reload();
            }else{
              alert('Something Went Wrong!')
            }
            
        },
        error: function(response){
            console.log(response);
        }
      });//ajax
    });

    $('#register-btn').on('click', function(e){
      e.preventDefault();
      var that = $(this);
      that.parent('.text-center').siblings('.my-3').children('.error-message').hide();
      that.parent('.text-center').siblings('.my-3').children('.loading').hide();
      that.parent('.text-center').siblings('.my-3').children('.sent-message').hide();
      var email     = that.parent('.text-center').siblings('.form-group').children('#email').val();
      var password  = that.parent('.text-center').siblings('.form-group').children('#password').val();
      var url       = that.parent('.text-center').siblings('.form-group').children('#url').val();
      var fname     = that.parent('.text-center').siblings('.form-group').children('#fname').val();
      var lname     = that.parent('.text-center').siblings('.form-group').children('#lname').val();
      var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
      var urlReg = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
      var passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
      if( !emailReg.test(email) ){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Please Provide a valid email!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        // that.parent('.text-center').siblings('.form-group').children('#email').css('border','1px solid red')
        return false;
      }
      if( !urlReg.test(url) ){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Please Provide a valid url!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        // that.parent('.text-center').siblings('.form-group').children('#url').css('border','1px solid red')
        return false;
      }
      if(fname.length < 1){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Please Provide a first name!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        return false
      }
      if(lname.length < 1){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Please Provide a last name!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        return false;
      }
      if( !passReg.test(password) ){
        that.parent('.text-center').siblings('.my-3').children('.error-message').html('Password must contain at least one digit, one lower case, one upper case and 8 charecters!');
        that.parent('.text-center').siblings('.my-3').children('.error-message').show();
        // that.parent('.text-center').siblings('.form-group').children('#url').css('border','1px solid red')
        return false;
      }
      $.ajax({
        beforeSend : function(xhr){
            that.parent('.text-center').siblings('.my-3').children('.loading').show();
        },
        url : "<?php echo site_url('front/login/ajax_register') ?>",
        type : 'POST',
        data : {
            'email'     : email,
            'password'  : password,
            'url'       : url,
            'fname'     : fname,
            'lname'     : lname,
        },
        success: function( data ){
            // console.log(data);
            var response = jQuery.parseJSON(data);
            if(response.code == 1){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.sent-message').show();
              setTimeout(
                function() 
                {
                  location.reload();
                }, 2000);
            }
            if(response.code == 3){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.error-message').html('Url is returning 404 status! Try putting www infront of your domain name. If that does not work check your domain.');
              that.parent('.text-center').siblings('.my-3').children('.error-message').show();
            }
            if(response.code == 5){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.error-message').html('Url already exists!');
              that.parent('.text-center').siblings('.my-3').children('.error-message').show();
            }
            if(response.code == 9){
              that.parent('.text-center').siblings('.my-3').children('.loading').hide();
              that.parent('.text-center').siblings('.my-3').children('.error-message').html('Email already exists!');
              that.parent('.text-center').siblings('.my-3').children('.error-message').show();
            }
        },
        error: function(response){
            console.log(response);
        }
      });//ajax
    });

    $('#email').on('change', function(e){
      // e.preventDefault();
      var that = $(this);
      that.parent('.form-group').siblings('.text-center').children('#register-btn').show();
      that.parent('.form-group').siblings('.my-3').children('.error-message').hide();
      that.parent('.form-group').siblings('.my-3').children('.loading').hide();
      that.parent('.form-group').siblings('.my-3').children('.sent-message').hide();
      var email     = that.val();
      var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
      if( !emailReg.test(email) ){
        that.parent('.form-group').siblings('.my-3').children('.error-message').html('Please Provide a valid email!');
        that.parent('.form-group').siblings('.my-3').children('.error-message').show();
        // that.parent('.form-group').siblings('.form-group').children('#email').css('border','1px solid red')
        return false;
      }
      $.ajax({
        beforeSend : function(xhr){
            that.parent('.form-group').siblings('.my-3').children('.loading').show();
        },
        url : "<?php echo site_url('front/login/check_email') ?>",
        type : 'POST',
        data : {
            'email'     : email,
        },
        success: function( data ){
            // console.log(data);
            var response = jQuery.parseJSON(data);
            if(response.code == 0){
              that.parent('.form-group').siblings('.my-3').children('.loading').hide();
              that.css('border','1px solid #ced4da');
            }
            if(response.code == 1){
              that.parent('.form-group').siblings('.my-3').children('.loading').hide();
              that.parent('.form-group').siblings('.my-3').children('.error-message').html('Email already exists!');
              that.parent('.form-group').siblings('.my-3').children('.error-message').show();
              that.css('border','1px solid red');
              that.parent('.form-group').siblings('.text-center').children('#register-btn').hide();
            }
            
        },
        error: function(response){
            console.log(response);
        }
      });//ajax
    });
    $('#url').on('change', function(e){
      // e.preventDefault();
      var that = $(this);
      that.parent('.form-group').siblings('.text-center').children('#register-btn').show();
      that.parent('.form-group').siblings('.my-3').children('.error-message').hide();
      that.parent('.form-group').siblings('.my-3').children('.loading').hide();
      that.parent('.form-group').siblings('.my-3').children('.sent-message').hide();
      var url     = that.val();
      var urlReg = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
      if( !urlReg.test(url) ){
        that.parent('.form-group').siblings('.my-3').children('.error-message').html('Please Provide a valid url!');
        that.parent('.form-group').siblings('.my-3').children('.error-message').show();
        // that.parent('.form-group').siblings('.form-group').children('#email').css('border','1px solid red')
        return false;
      }
      $.ajax({
        beforeSend : function(xhr){
            that.parent('.form-group').siblings('.my-3').children('.loading').show();
        },
        url : "<?php echo site_url('front/login/check_url') ?>",
        type : 'POST',
        data : {
            'url'     : url,
        },
        success: function( data ){
            // console.log(data);
            var response = jQuery.parseJSON(data);
            if(response.code == 0){
              that.parent('.form-group').siblings('.my-3').children('.loading').hide();
              that.css('border','1px solid #ced4da');
            }
            if(response.code == 1){
              that.parent('.form-group').siblings('.my-3').children('.loading').hide();
              that.parent('.form-group').siblings('.my-3').children('.error-message').html('Url already exists in our system!');
              that.parent('.form-group').siblings('.my-3').children('.error-message').show();
              that.css('border','1px solid red');
              that.parent('.form-group').siblings('.text-center').children('#register-btn').hide();
            }
            if(response.code == 5){
              that.parent('.form-group').siblings('.my-3').children('.loading').hide();
              that.parent('.form-group').siblings('.my-3').children('.error-message').html('Url is returning 404 status! Try putting www infront of your domain name. If that does not work check your domain.');
              that.parent('.form-group').siblings('.my-3').children('.error-message').show();
              that.css('border','1px solid red');
              that.parent('.form-group').siblings('.text-center').children('#register-btn').hide();
            }
            
        },
        error: function(response){
            console.log(response);
        }
      });//ajax
    });
  } );
</script>
</body>

</html>