<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SportScore</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo site_url('/') ?>assets/img/favicon.jpg" rel="icon">
  <link href="<?php echo site_url('/') ?>assets/img/favicon.jpg.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo site_url('/') ?>assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo site_url('/') ?>assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo site_url('/') ?>assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Vesperr - v4.1.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <h1><a href="<?php echo site_url() ?>">SportScore</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link scrollto " href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li><a class="nav-link scrollto" href="#pricing">Pricing</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <?php
            if( isset($this->session->userdata['spsc23xyzsdfretw89lk_user_id']) ){
          ?>
              <li class="dropdown"><a href="#"><span><?php echo $header_data_array['first_name'] ?></span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li><a href="#">Drop Down 1</a></li>
                  <li><a href="#">Drop Down 2</a></li>
                  <li><a href="#">Drop Down 3</a></li>
                  <li><a href="<?php echo site_url('front/login/logout') ?>" id="logout-btn">Logout</a></li>
                </ul>
              </li>
          <?php
            }else{
          ?>
              <li><a class="nav-link scrollto" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
          <?php
            }
          ?>
          <li><a class="getstarted scrollto" href="#" data-bs-toggle="modal" data-bs-target="#registerModal">Get Started</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->


<!-- Login Modal -->
    <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="loginModalLabel">Login</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <section id="contact" class="contact">
              <div class="container">

              <div class="section-title" data-aos="fade-up">
                <h2>Login</h2>
              </div>

                <div class="row">

                  <div class="col-lg-12 col-md-12" data-aos="fade-up" data-aos-delay="300">
                    <form class="php-email-form">
                      <div class="form-group">
                        <input type="text" name="username" class="form-control" id="username" placeholder="Userame or Email" required>
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" name="password" id="login-password" placeholder="Password" required>
                      </div>
                      
                      <div class="my-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Login Succesful. Thank you! Please Wait!</div>
                      </div>
                      <div class="text-center">
                        <button type="submit" id="login-btn">Login</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </section>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>

    <!-- Register Modal -->
    <div class="modal fade" id="registerModal" tabindex="-1" aria-labelledby="registerModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="registerModalLabel">Register your website.</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <section id="contact" class="contact">
              <div class="container">

              <div class="section-title" data-aos="fade-up">
                <h2>Register</h2>
              </div>

                <div class="row">

                  <div class="col-lg-12 col-md-12" data-aos="fade-up" data-aos-delay="300">
                    <form class="php-email-form" autocomplete="off">
                      <div class="form-group">
                        <input type="text" name="email" class="form-control" id="email" placeholder="info@example.com" autocomplete="off" required>
                      </div>
                      <div class="form-group">
                        <input type="text" name="url" class="form-control" id="url" placeholder="https://example.com" required>
                      </div>
                      <div class="form-group">
                        <input type="text" name="fname" class="form-control" id="fname" placeholder="John" required>
                      </div>
                      <div class="form-group">
                        <input type="text" name="lname" class="form-control" id="lname" placeholder="Doe" required>
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                      </div>
                      
                      <div class="my-3">
                        <div class="loading">Loading</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Registration Succesful. Thank you!</div>
                      </div>
                      <div class="text-center">
                        <button type="submit" id="register-btn">Register</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </section>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

