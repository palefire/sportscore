<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller{
	public $header_data_array = array();
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
  //       if( !isset($this->session->userdata['spsc23xyzsdfretw89lk_user_id']) ){
		// 	return redirect('login');
		// 	die();
		// } 
        $this->load->model('front/frontmodel');
  //       $user_token = $this->session->userdata['spsc23xyzsdfretw89lk_user_id'];
		// $user_details = $this->frontmodel->get_user_info( $user_token );
		// $user_status = $user_details['user_status'];
		// if( $user_status == '0' ){
		// 	return site_url('verify');
		// }
  //      	$user_id =  $user_details['user_id'];
        if( isset( $this->session->userdata['spsc23xyzsdfretw89lk_user_id'] ) ){
        	$user_info = $this->frontmodel->get_user_info($this->session->userdata['spsc23xyzsdfretw89lk_user_id']);
        	$this->header_data_array['first_name'] 	= $user_info['first_name'];
        	$this->header_data_array['last_name'] 	= $user_info['last_name'];
        }
        
        
	}

	/*
	|--------------------------------------------------------------------------
	| Home function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		
		$this->load->view('header', array(
			'header_data_array'	 => $this->header_data_array,
		));
		$this->load->view('home');
		$this->load->view('footer');
		
	}//fn

	// public function check_header(){
	// 	$file = 'https://www.walledsports.com';
	// 	$file_headers = @get_headers($file);
	// 	echo '<pre>';
	// 	print_r($file_headers);
	// 	if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
	// 	    // $exists = false;
	// 	    echo 'URL don\'t exist';

	// 	}
	// 	else {
	// 	    // $exists = trueha
	// 	    		    echo 'URL  exist';
	// 	}
	// }//fn

	/*
	|--------------------------------------------------------------------------
	| Contact function
	|--------------------------------------------------------------------------
	*/


	

}//class