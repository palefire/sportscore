<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

       
        $this->load->model('admin/adminmodel','adminmodel');
        $this->load->model('front/frontmodel','frontmodel');
        
	}

	/*
	|--------------------------------------------------------------------------
	| Login function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		// if( isset($this->session->userdata['spsc23xyzsdfretw89lk_user_id']) ){
		// 	return redirect(site_url());
		// }
		
		// $this->load->view('login');
		return redirect( site_url() );
		
	}//fn

	public function verify_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		// $kpln = $this->input->post('kpln');
		if( isset($this->session->userdata['spsc23xyzsdfretw89lk_user_id']) ){
			$return_array = array(
				'code'		=> 5,
				'message'	=> 'Already Logged in!'
			);
			echo json_encode($return_array);
			die();
		}
		$login_success = $this->frontmodel->checklogin( $username, $password);
		// print_r($verified);
		
		if( $login_success ){
			$website_id = $login_success['website_id'];
			$website_status = $login_success['verified'];
			$this->session->set_userdata( 'spsc23xyzsdfretw89lk_user_id', $website_id );
			$return_array = array(
				'code'		=> 1,
				'message'	=> 'Login Succesful'
			);
			echo json_encode($return_array);
			die();
		}else{
			$return_array = array(
				'code'		=> 0,
				'message'	=> 'Login Failed'
			);
			echo json_encode($return_array);
			die();
		}
	}//fn

	public function ajax_logout(){
		
		$this->session->unset_userdata('spsc23xyzsdfretw89lk_user_id');
		
		$return_array = array(
				'code'		=> 1,
				'message'	=> 'Login Succesful'
			);
		echo json_encode($return_array);
	}//fn
	public function logout(){
		
		$this->session->unset_userdata('spsc23xyzsdfretw89lk_user_id');
		return redirect( site_url() );
	}//fn

	public function ajax_register(){
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$url 		= $this->input->post('url');
		$fname 		= $this->input->post('fname');
		$lname 		= $this->input->post('lname');

		$check_email 	= $this->frontmodel->check_email_exist( $email );
		if( $check_email ){
			$return_array = array(
				'code'		=> 9,
				'message'	=> 'Email exists in system!'
			);
			echo json_encode($return_array);
			die();
		}
		$check_url 	= $this->frontmodel->check_url_exist( $url );
		if( $check_url ){
			$return_array = array(
				'code'		=> 5,
				'message'	=> 'Url exists in system!'
			);
			echo json_encode($return_array);
			die();
		}
		$file_headers = @get_headers($url);
		if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
			$return_array = array(
				'code'		=> 3,
				'message'	=> 'Url showing 404 status'
			);
			echo json_encode($return_array);
			die();
		}
		$register_website_id = $this->frontmodel->register_website_to_db( $email, $password, $url, $fname, $lname );
		if( $register_website_id ){
			$this->session->set_userdata( 'spsc23xyzsdfretw89lk_user_id', $register_website_id );
			$return_array = array(
				'code'		=> 1,
				'message'	=> 'Registration Succesful'
			);
			echo json_encode($return_array);
			die();
		}else{
			$return_array = array(
				'code'		=> 0,
				'message'	=> 'Registration Failed!'
			);
			echo json_encode($return_array);
			die();

		}

	}//fn

	public function register_user_to_db(){
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$username 	= $this->input->post('username');
		
		$register_user_token = $this->frontmodel->register_user_to_db( $email, $password, $username, $_SERVER );
		if( $register_user_token ){
			$this->session->set_userdata( 'spsc23xyzsdfretw89lk_user_id', $register_user_token );
			return redirect(site_url('verify'));
		}else{
			$this->session->set_flashdata( 'DB_ERROR' , 'OOPS! There was a database error. Please wait and try after sometime.' );
			return redirect(site_url('signup'));

		}
	}//fn

	public function check_email(){
		$email 		= $this->input->post('email');
		$check_result 	= $this->frontmodel->check_email_exist( $email );
		if( $check_result ){
			$return_array = array(
				'code'		=> 1,
				'message'	=> 'Email exists in system!'
			);
			echo json_encode($return_array);
			die();
		}else{
			$return_array = array(
				'code'		=> 0,
				'message'	=> 'Email does not exists in system!'
			);
			echo json_encode($return_array);
			die();
		}
	}//fn

	public function check_url(){
		$url 		= $this->input->post('url');
		$check_result 	= $this->frontmodel->check_url_exist( $url );
		if( $check_result ){
			$return_array = array(
				'code'		=> 1,
				'message'	=> 'Url exists in system!'
			);
			echo json_encode($return_array);
			die();
		}else{
			$file_headers = @get_headers($url);
			if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
				$return_array = array(
					'code'		=> 5,
					'message'	=> 'Url showing 404 status'
				);
				echo json_encode($return_array);
				die();
			}else{
				$return_array = array(
					'code'		=> 0,
					'message'	=> 'Url is live and does not exist i the system!'
				);
				echo json_encode($return_array);
				die();
			}
			
		}
	}//fn


	public function verify_email(){
		$user_token = $this->session->userdata['spsc23xyzsdfretw89lk_user_id'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_status = $user_details['user_status'];
		if( $user_status == '1' ){
			return redirect(site_url());
			die();
		}
		$this->load->view('verify_email');
	}//fn

	public function verify_email_in_db(){
		$user_token = $this->session->userdata['spsc23xyzsdfretw89lk_user_id'];
		$otp 		= $this->input->post('otp');
		$verify_email = $this->frontmodel->verify_email( $user_token, $otp );
		if( $verify_email ){
			return redirect(site_url('profile-info'));
		}else{
			$this->session->set_flashdata( 'login_failed' , 'Incorrect Otp.' );
			return redirect(site_url('verify'));
		}
	}//fn

	public function get_extra_profile_info(){
		$this->load->view('extra_profile_info');
	}//fn

	public function save_extra_profile_info_to_db(){
		$user_token = $this->session->userdata['spsc23xyzsdfretw89lk_user_id'];
		$fname 		= $this->input->post('fname');
		$lname 		= $this->input->post('lname');
	}//fn
}